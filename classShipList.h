#ifndef CLASSSHIPLIST_H
#define CLASSSHIPLIST_H

#include <iostream>
#include <fstream>
#include <list>
#include <string.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;

class Point
{
private:
    int coorX, coorY;
public:
    Point() {}
    Point(int x, int y) : coorX(x), coorY(y) {}

    inline int getX() { return coorX; };

    inline void setX(int x) { coorX = x; };

    inline int getY() { return coorY; };

    inline void setY(int y) { coorY = y; };
};

class Ship
{
private:

    Point point;
    int limitCoor;

public:

    Ship(Point &point, int limitCoor)
    {
        this->point = point;
        this->limitCoor = limitCoor;
    }

    inline int getXcoor()
    {
        return point.getX();
    }

    inline int getYcoor()
    {
        return point.getY();
    }

    inline void setXcoor(int coorX)
    {
       point.setX(coorX);
    }

    inline void setYcoor(int coorY)
    {
        point.setY(coorY);
    }

    void moveUp(int distance)
    {
      while(distance + getYcoor() < limitCoor && distance > 0)
      {
         distance--;
      }
      setYcoor(distance + getYcoor());
    }

    void moveDown(int distance)
    {
      while(getYcoor() - distance > 0 && distance > 0)
      {
         distance--;
      }
      setYcoor(getYcoor() - distance);
    }

    void moveRight(int distance)
    {
      while(distance + getXcoor() < limitCoor && distance > 0)
      {
         distance--;
      }
      setXcoor(distance + getXcoor());
    }

    void moveLeft(int distance)
    {
      while(getXcoor() - distance > 0 && distance > 0)
      {
         distance--;
      }
      setYcoor(getXcoor() - distance);
    }
};

class ShipList : public list <Ship>
{
private:
    const string fileName;

    list <Ship> shipList;

    bool fileValid(const string &fileName)
    {
        ifstream file(fileName);
        char line[10];
        file.getline(line, 10);
        if(fileName.find("OlimpiadaFile.txt") && strcmp(line, "Ships") == 0) {return true;}
        else {return false;}
    }

public:

    ShipList(const string &fileName)
    {
        if(!fileValid(fileName))
        {
            cout << "file isn't correct";
        }
        else
        {
            load(fileName);
        }
    }

    void load(const string &fileName)
    {
       ifstream file(fileName);       
       char line[10];
       file.getline(line, 10);
       file.getline(line, 10);
       int limitCoor = atoi(&line[0]);
       for(int counter = 0; counter < limitCoor; counter++)
       {
           file.getline(line, 10);
           Point point(atoi(&line[0]), atoi(&line[2]));
           Ship object(point, limitCoor);
           shipList.push_back(object);
       }
    }    

    int minCountOfMovesTo1column()
        {
             int moveCounter = 0;
             makeShipsDiffrentXcoor(moveCounter);
             for(int counter = 0; counter < shipList.size(); counter++)
             {


             }
        }

    int makeShipsDiffrentXcoor(int moveCounter)//Нужно очень много анализа позицицй всех кораблей!!!!!!!!!!!!! Поэтому мат модель решения задачи никак не может быть правильной
    //Нужно каким-то образом одним кораблем проверять пространство в радиусе 1 клетки и двигаться только тогда, когда его движению ничто не мешает!!!!!!
    {

      return moveCounter;
    }
};
#endif // CLASSSHIPLIST_H
